@extends('layout.master')
@section('judul')
    Halaman Detail Cast
@endsection

@section('content')
    <h1>{{$cast->nama}} ({{$cast->umur}})</h1>
    <p>{{$cast->bio}}</p>
@endsection