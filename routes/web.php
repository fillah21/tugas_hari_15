<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/data-tables', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@move');

Route::post('/welcome', 'AuthController@welcome');

// Membuat Route untuk CRUD cast
// Mengarah ke form create data
Route::get('/cast/create', 'CastController@create');
// Menyimpan data ke table cast
Route::post('/cast', 'CastController@store');

// Menampilkan semua data
Route::get('/cast', 'CastController@index');
// Detai data
Route::get('/cast/{cast_id}', 'CastController@show');

// Mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Update data ke table cast
Route::put('/cast/{cast_id}', 'CastController@update');

// Delete data 
Route::delete('/cast/{cast_id}', 'CastController@destroy');
